import * as React from 'react';
import { Link } from 'react-router-dom';
import firebase from '../firebase';
import Typography from '@material-ui/core/Typography';

class Main extends React.Component<any, any> {
    private ref: any;
    private unsubscribe: any;

    constructor(props: any) {
        super(props);
        this.ref = firebase.firestore().collection('boards');
        this.unsubscribe = null;
        this.state = {
        boards: []
        };
    }

    onCollectionUpdate = (querySnapshot: any) => {
        const boards: any = [];
        querySnapshot.forEach((doc: any) => {
        const { title, description, author } = doc.data();
        boards.push({
            key: doc.id,
            doc, // DocumentSnapshot
            title,
            description,
            author,
        });
        });
        this.setState({
        boards
    });
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }

    render() {
        return (
            <React.Fragment>
                <Typography>
                    BOARD LIST
                </Typography>
                <div>
                    <h4><Link to="/create">Add Board</Link></h4>
                    <table>
                    <thead>
                        <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Author</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.boards.map((board: any) =>
                        <tr>
                            <td><Link to={`/show/${board.key}`}>{board.title}</Link></td>
                            <td>{board.description}</td>
                            <td>{board.author}</td>
                        </tr>
                        )}
                    </tbody>
                    </table>
                </div>
            </React.Fragment>
        );
    }
}

export default Main;