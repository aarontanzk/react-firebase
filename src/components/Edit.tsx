import * as React from 'react';
import firebase from '../firebase';
import { Link } from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default class Edit extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            key: '',
            title: '',
            description: '',
            author: ''
          };
    }

    componentDidMount() {
        const ref = firebase.firestore().collection('boards').doc(this.props.match.params.id);
        ref.get().then((doc) => {
        if (doc.exists) {
            const board = doc.data();
            this.setState({
            key: doc.id,
            title: board!.title,
            description: board!.description,
            author: board!.author
            });
        } else {
            console.log("No such document!");
        }
        });
    }

    handleInputChange = (e: any) => {
        const {name, value} = e.target
        this.setState({
            [name]: value
        })
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        const { title, description, author } = this.state;

        const updateRef = firebase.firestore().collection('boards').doc(this.state.key);
        updateRef.set({
            title,
            description,
            author
        }).then((docRef) => {
        this.setState({
            key: '',
            title: '',
            description: '',
            author: ''
        });
        this.props.history.push("/show/"+this.props.match.params.id)
        })
        .catch((error) => {
        console.error("Error adding document: ", error);
        });
    }

    render() {
        return (
            <React.Fragment>
                <Typography>
                    EDIT BOARD
                </Typography>
                <h4><Link to="/" >Book List</Link></h4>
                <h4><Link to={`/show/${this.state.key}`}>Board List</Link></h4>
                <FormControl>
                    <TextField
                        name='title'
                        label='Title'
                        onChange={this.handleInputChange}
                        value={this.state.title}
                    />
                    <TextField
                        name='description'
                        label='Description'
                        onChange={this.handleInputChange}
                        value={this.state.description}
                    />
                    <TextField
                        name='author'
                        label='Author'
                        onChange={this.handleInputChange}
                        value={this.state.author}
                    />
                </FormControl>
                <Button onClick={this.onSubmit}> Submit </Button>
            </React.Fragment>
        )
    }
}
