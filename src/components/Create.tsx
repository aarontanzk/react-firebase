import React from 'react';
import ReactDOM from 'react-dom';
import firebase from '../firebase';
import { Link } from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default class Create extends React.Component<any, any> {
    private ref: any;

    constructor(props: any) {
        super(props);
        this.ref = firebase.firestore().collection('boards');
        this.state = {
          title: 'initialcrwateTITLE',
          description: 'initialcrwateDESC',
          author: 'initialcrwateauthro'
        };
    }

    handleInputChange = (e: any) => {
        const {name, value} = e.target
        this.setState({
            [name]: value
        })
    }

    onSubmit = () => {
        const { title, description, author } = this.state;
        this.ref.add({
            title,
            description,
            author
        }).then((docRef: any) => {
            this.setState({
                title: '',
                description: '',
                author: ''
            });
            this.props.history.push("/")
        })
        .catch((error: any) => {
          console.error("Error adding document: ", error);
        });
    }
      
    render() {
        const { title, description, author } = this.state;
        return (
            <React.Fragment>
                <Typography>
                    ADD BOARD
                </Typography>
                <h4><Link to="/" >Book List</Link></h4>
                <FormControl>
                    <TextField
                        name='title'
                        label='Title'
                        onChange={this.handleInputChange}
                        value={title}
                    />
                    <TextField
                        name='description'
                        label='Description'
                        onChange={this.handleInputChange}
                        value={description}
                    />
                    <TextField
                        name='author'
                        label='Author'
                        onChange={this.handleInputChange}
                        value={author}
                    />
                </FormControl>
                <Button onClick={this.onSubmit}> Submit </Button>
            </React.Fragment>
        );
    }
}