import React from 'react';
import firebase from '../firebase';
import { Link } from 'react-router-dom';
export default class Show extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            board: {},
            key: ''
          };
    }

    componentDidMount() {
        const ref = firebase.firestore().collection('boards').doc(this.props.match.params.id);
        ref.get().then((doc) => {
          if (doc.exists) {
            this.setState({
              board: doc.data(),
              key: doc.id,
              isLoading: false
            });
          } else {
            console.log("No such document!");
          }
        });
      }
    
      delete(id: any){
        firebase.firestore().collection('boards').doc(id).delete().then(() => {
          console.log("Document successfully deleted!");
          this.props.history.push("/")
        }).catch((error) => {
          console.error("Error removing document: ", error);
        });
      }

    render() {
        return (
            <div >
            <div >
              <div>
              <h4><Link to="/">Board List</Link></h4>
                <h3>
                  {this.state.board.title}
                </h3>
              </div>
              <div>
                <dl>
                  <dt>Description:</dt>
                  <dd>{this.state.board.description}</dd>
                  <dt>Author:</dt>
                  <dd>{this.state.board.author}</dd>
                </dl>
                <Link to={`/edit/${this.state.key}`}>Edit</Link>&nbsp;
                <button onClick={this.delete.bind(this, this.state.key)}>Delete</button>
              </div>
            </div>
          </div>
        )
    }
}