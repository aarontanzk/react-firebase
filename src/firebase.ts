import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const firebaseConfig = {
    apiKey: "AIzaSyDRH-kfW9L63eACjxKn25fViFjGnMYG_8E",
    authDomain: "react-firebase-atzk.firebaseapp.com",
    databaseURL: "https://react-firebase-atzk.firebaseio.com",
    projectId: "react-firebase-atzk",
    storageBucket: "react-firebase-atzk.appspot.com",
    messagingSenderId: "250521086297",
    appId: "1:250521086297:web:e737fe5828951eb4"
  };

firebase.initializeApp(firebaseConfig);

firebase.firestore().settings(settings);

export default firebase;